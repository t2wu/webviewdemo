//
//  ViewController.h
//  WebViewDemo
//
//  Created by Timothy Wu on 2015/2/18.
//  Copyright (c) 2015 Timothy Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UILabel *label;



- (IBAction)runJSScript:(id)sender;


@end

