//
//  AppDelegate.h
//  WebViewDemo
//
//  Created by Timothy Wu on 2015/2/18.
//  Copyright (c) 2015 Timothy Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

