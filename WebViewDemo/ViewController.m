//
//  ViewController.m
//  WebViewDemo
//
//  Created by Timothy Wu on 2015/2/18.
//  Copyright (c) 2015 Timothy Wu. All rights reserved.
//

#import "ViewController.h"

@import JavaScriptCore;

@interface ViewController ()

@property (nonatomic, strong) JSContext *ctx;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.webview.delegate = self;
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html"];
    [self.webview loadRequest:[NSURLRequest requestWithURL:url]];
}


#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // reference: http://blog.impathic.com/post/64171814244/true-javascript-uiwebview-integration-in-ios7
    // Prepare some blocks for Objective-C to be called
    JSContext *ctx = [self.webview valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    
    NSAssert([ctx isKindOfClass:[JSContext class]], @"could not find context in web view");
    
    self.ctx = ctx;
    ctx[@"sayHelloToObjC"] = ^(JSValue *msg, JSValue *callback) {
        NSLog(@"Running say hello to objC");
        NSString * msgC = msg.toString; // You can convert to many types, maybe JSON?
        [self sayHelloToObjC:msgC callback:callback];
//        [callback callWithArguments:@[@"hi"]];
    };
}

#pragma mark - Objective-C and JS bridges
- (void)callJS
{
    JSValue * jsrun = self.ctx[@"window"][@"jsrun"];
    [jsrun callWithArguments:@[@(123)]];
    //[self.webview stringByEvaluatingJavaScriptFromString:@"jsrun()"];
    
}

- (void)sayHelloToObjC:(NSString *)msg callback:(JSValue *)callback
{
    NSLog(@"Received message: %@", msg);
//    self.label.text = msg;
    [callback callWithArguments:@[@"abc"]];
}

#pragma mark - actions
- (IBAction)runJSScript:(id)sender
{
    [self callJS];
}


@end
