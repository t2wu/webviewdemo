//
//  main.m
//  WebViewDemo
//
//  Created by Timothy Wu on 2015/2/18.
//  Copyright (c) 2015 Timothy Wu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
